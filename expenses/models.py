from django.db import models


class Category(models.Model):
    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"


class Expense(models.Model):
    name = models.CharField(
        max_length=255
    )

    amount = models.DecimalField(
        max_digits=7,
        decimal_places=2
    )

    timestamp = models.DateTimeField(
        auto_now_add=True
    )

    category = models.ForeignKey(
        to=Category,
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return f"{self.name} ({self.amount} BYN)"

