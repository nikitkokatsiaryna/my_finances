from django.urls import path
from .views import HomePage
from django.views import generic


urlpatterns = [
    path('', HomePage.as_view(), name='home'),
    path('about/', generic.TemplateView.as_view(
        template_name="main_app/about.html"
    ), name='about')
]
