from django.db import models
from django.contrib.auth.models import User


class BaseUserProfile(models.Model):
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE
    )
    avatar = models.ImageField(
        upload_to='profile'
    )

    class Meta:
        abstract = True


class CommonUserProfile(BaseUserProfile):
    pass


class PremiumUserProfile(BaseUserProfile):
    account = models.BigIntegerField(default=0)
