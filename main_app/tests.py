from django.test import TestCase, Client


class UrlsTets(TestCase):
    def setUp(self):
        self.client = Client()

    def test_home_page_status(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_about_page_status(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)


class ContentsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_home_page_status(self):
        response = self.client.get('')
        self.assertContains(response, 'Main Page')

    def test_about_page_status(self):
        response = self.client.get('/about/')
        self.assertContains(response, 'About')
