from django.contrib import admin

from profiles import models as prof_models


admin.site.register(prof_models.CommonUserProfile)
admin.site.register(prof_models.PremiumUserProfile)
