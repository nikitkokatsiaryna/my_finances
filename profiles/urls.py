from django.urls import path
from django.views import generic
from django.views.generic import DetailView


from profiles.models import CommonUserProfile


urlpatterns = [
    path('<int:pk>/', DetailView.as_view(
        template_name="profiles/profile.html",
        model=CommonUserProfile
    ), name='profile'),
]
