from .models import Expense, Category
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, CreateView, UpdateView
from expenses import forms


class ExpenseListView(ListView):
    template_name = "expenses/expenses_list.html"
    model = Expense


class ExpenseDetailView(DeleteView):
    template_name = "expenses/detail.html"
    model = Expense


class ExpenseCreateView(CreateView):
    template_name = 'expenses/create.html'
    model = Expense
    form_class = forms.ExpensesCreateForm
    success_url = reverse_lazy('expenses:list')


class ExpenseUpdateView(UpdateView):
    template_name = "expenses/update.html"
    model = Expense
    form_class = forms.ExpenseUpdateForm

    def get_success_url(self):
        return reverse_lazy(
            'expenses:detail', kwargs={'pk': self.kwargs['pk']}
        )


class ExpenseDeleteView(DeleteView):
    template_name = 'expenses/delete.html'
    model = Expense
    success_url = reverse_lazy('expenses:list')


class CategoryListView(ListView):
    template_name = "expenses/category.html"
    model = Category


class CategoryDetailView(DeleteView):
    template_name = 'expenses/category_detail.html'
    model = Category
    # success_url = reverse_lazy('expenses:category_detail')


class CategoryCreateView(CreateView):
    template_name = "expenses/category_create.html"
    model = Category
    form_class = forms.CategoryCreateForm
    success_url = reverse_lazy('expenses:category_list')


class CategoryUpdateView(UpdateView):
    template_name = "expenses/category_update.html"
    model = Category
    form_class = forms.CategoryUpdateForm

    def get_success_url(self):
        return reverse_lazy(
            'expenses:category_detail', kwargs={'pk': self.kwargs['pk']}
        )


class CategoryDeleteView(DeleteView):
    template_name = 'expenses/category_delete.html'
    model = Category
    success_url = reverse_lazy('expenses:category_list')
