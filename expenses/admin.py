from django.contrib import admin
from .models import Expense, Category


class ExpenseAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'amount',
        'timestamp',
        'category'
    ]


admin.site.register(Expense, ExpenseAdmin)
admin.site.register(Category)
