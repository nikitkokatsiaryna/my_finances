from django import forms
from .models import *


class ExpensesCreateForm(forms.ModelForm):

    class Meta:
        model = Expense
        exclude = ['timestamp']


class ExpenseUpdateForm(forms.ModelForm):

    class Meta:
        model = Expense
        fields = "__all__"


class CategoryCreateForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = '__all__'


class CategoryUpdateForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = "__all__"
