from django.urls import path
from expenses import views


urlpatterns = [
    path('expenses', views.ExpenseListView.as_view(), name='list'),
    path('<int:pk>/detail/', views.ExpenseDetailView.as_view(), name='detail'),
    path('create/', views.ExpenseCreateView.as_view(), name='create'),
    path('<int:pk>/update/', views.ExpenseUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', views.ExpenseDeleteView.as_view(), name='delete'),

    path('category/', views.CategoryListView.as_view(), name="category_list"),
    path('category/<int:pk>/detail/', views.CategoryDetailView.as_view(), name="category_detail"),
    path('category/create/', views.CategoryCreateView.as_view(), name='category_create'),
    path('category/<int:pk>/update/', views.CategoryUpdateView.as_view(), name='category_update'),
    path('category/<int:pk>/delete/', views.CategoryDeleteView.as_view(), name='category_delete')
]
